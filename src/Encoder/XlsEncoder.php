<?php

/**
 * @file
 * Contains \Drupal\csv_serialization\Encoder\CsvEncoder.
 */

namespace Drupal\xls_serialization\Encoder;

use Symfony\Component\Serializer\Encoder\DecoderInterface;
use Symfony\Component\Serializer\Encoder\EncoderInterface;
use PHPExcel;
use PHPExcel_Writer_Excel2007;
use Drupal\Component\Utility\Html;
use Drupal\Component\Serialization\Exception\InvalidDataTypeException;

/**
 * Adds XLS encoder support for the Serialization API.
 */
class XlsEncoder implements EncoderInterface, DecoderInterface {


  /**
   * Indicates the character used to delimit fields. Defaults to ",".
   *
   * @var string
   */
  protected $delimiter;

  /**
   * Indicates the character used for field enclosure. Defaults to '"'.
   *
   * @var string
   */
  protected $enclosure;

  /**
   * Indicates the character used for escaping. Defaults to "\".
   *
   * @var string
   */
  protected $escapeChar;

  /**
   * The format that this encoder supports.
   *
   * @var string
   */
  protected static $format = 'xlsx';

  /**
   * Constructs the class.
   */
  public function __construct() {

  }

  /**
   * {@inheritdoc}
   */
  public function supportsEncoding($format) {
    return $format == static::$format;
  }

  /**
   * {@inheritdoc}
   */
  public function supportsDecoding($format) {
    return $format == static::$format;
  }

  /**
   * {@inheritdoc}
   *
   * Uses HTML-safe strings, with several characters escaped.
   */
  public function encode($data, $format, array $context = array()) {
    switch (gettype($data)) {
      case "array":
        break;
      case 'object':
        $data = (array) $data;
        break;
      // May be bool, integer, double, string, resource, NULL, or unknown.
      default:
        $data = array($data);
        break;
    }

    try {
      $headers = array_keys(reset($data));
      array_unshift($data, $headers);
      $objPHPExcel = new PHPExcel();
      $objPHPExcel->setActiveSheetIndex(0);
      $objPHPExcel->getActiveSheet()->fromArray($data);
      $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
      $file_name = file_directory_temp() . '/' . uniqid() . '.xlsx';
      $objWriter->save($file_name);
      $f = fopen($file_name, 'r+');
      $output = fread($f, filesize($file_name));
      unlink($file_name);
      return $output;
    } catch (\Exception $e) {
      throw new InvalidDataTypeException($e->getMessage(), $e->getCode(), $e);
    }
  }


  /**
   * Formats a single value for a given CSV cell.
   *
   * @param string $value
   *   The raw value to be formatted.
   *
   * @return string
   *   The formatted value.
   *
   */
  protected function formatValue($value) {
    // @todo Make these filters configurable.
    $value = Html::decodeEntities($value);
    $value = strip_tags($value);
    $value = trim($value);
    $value = utf8_decode($value);

    return $value;
  }

  /**
   * {@inheritdoc}
   */
  public function decode($data, $format, array $context = array()) {
    $result = Reader::createFromString($data);
    //TODO:Fix this function
    return $result->fetchAssoc(0, array($this, 'expandRow'));
  }


  /**
   * {@inheritdoc}
   */
  public static function getFileExtension() {
    return static::$format;
  }

}
